package people;

import java.util.ArrayList;
import java.util.List;

public class Person {
    private String firstName;

    private String lastName;
    private List<Pet> pets;
    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.pets = new ArrayList<>();
    }

    public int getYoungestPet() {
        return pets.stream()
                .mapToInt(Pet::age)
                .min()
                .orElse(Integer.MAX_VALUE);
    }

    public Person addPet(PetType petType, String name, int age) {
        pets.add(new Pet(petType, name, age));
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public List<Pet> getPets() {
        return pets;
    }
}
